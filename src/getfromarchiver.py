import logging, requests, time, json
import sys
import tzlocal
from datetime import datetime

# Set logger to print on stdout
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

class GetFromArchiver(object):
    def __init__(self, addr):
        self.address = addr

    def fetchData(self, tRange, pvName, format='json'):
        params = {
            'pv': pvName,
            'from': self.convertTime(tRange[0]),
            'to': self.convertTime(tRange[1]),
        }
        logger.info("Downloading '%s' from %s", pvName, self.address)
        logger.info("Time range: %s -- %s", time.strftime('%Y-%m-%d %T.000%z', time.localtime(tRange[0])),
                    time.strftime('%Y-%m-%d %T.000%z', time.localtime(tRange[1])))
        req = requests.get('%s/data/getData.%s' % (self.address, format), params=params)
        if not req:
            raise RuntimeError('Failed fetch %s from %s, got %d!' % (pvName, self.address, req.status_code))
        logger.info("Download finished!")
        data = req.json()[0]
        # Create a file in JSON
        # with open('data.txt', 'w') as outfile:
        # json.dump(data, outfile, sort_keys=True, indent=4, separators=(',', ': '))

        return data

    def convertTime(self, tRange):
        return datetime.fromtimestamp(time.mktime(time.localtime(tRange)), tzlocal.get_localzone()).strftime(
            '%Y-%m-%dT%T.000%z')
