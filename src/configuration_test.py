import unittest

from configuration import *


class MyTestCase(unittest.TestCase):
    def test_readJsonConfiguration(self):
        configuration = Configuration("../resources/configurationFile.json")
        configuration.load()

        pedestalStart = configuration.getPedestalStart()
        pedestalEnd = configuration.getPedestalEnd()
        studyStart = configuration.getStudyStart()
        studyEnd = configuration.getStudyEnd()

        self.assertEqual("2019-03-06 20:13:12", pedestalStart)
        self.assertEqual("2019-03-06 20:13:14", pedestalEnd)
        self.assertEqual("2019-03-06 20:13:12", studyStart)
        self.assertEqual("2019-03-06 20:13:14", studyEnd)

    def test_readDiamonds(self):
        configuration = Configuration("../resources/configurationFile.json")
        configuration.load()

        configurationDiamondMap = configuration.getDiamondsLabels()

        testDiamondsMap = {
            'DCU1ch0': "SVD_BW_60",
            'DCU1ch1': "SVD_BW_120",
            'DCU1ch2': "SVD_BW_240",
            'DCU1ch3': "SVD_BW_300"
        }
        self.assertEqual(testDiamondsMap['DCU1ch0'], configurationDiamondMap['DCU1ch0'])
        self.assertEqual(testDiamondsMap['DCU1ch1'], configurationDiamondMap['DCU1ch1'])
        self.assertEqual(testDiamondsMap['DCU1ch2'], configurationDiamondMap['DCU1ch2'])
        self.assertEqual(testDiamondsMap['DCU1ch3'], configurationDiamondMap['DCU1ch3'])

    def test_readDataFiles(self):
        configuration = Configuration("../resources/configurationFile.json")
        configuration.load()

        dataFiles = configuration.getDataFiles()

        self.assertListEqual(["file1", "file2", "file3"], dataFiles)


if __name__ == '__main__':
    unittest.main()
