from datetime import datetime, timedelta

import gspread
from oauth2client.service_account import ServiceAccountCredentials

from diamond import *
from configuration import *
from analysisCore import *


def generateConfigurationJson(startTime, endTime):
    diamondsDefinition = {
        "BP_FW_145": "BP_FW_145",
        "BP_FW_35": "BP_FW_35",
        "BP_BW_145": "BP_BW_145",
        "BP_BW_35": "BP_BW_35",
        "SVD_FW_0": "SVD_FW_0",
        "SVD_FW_180": "SVD_FW_180",
        "SVD_BW_0": "SVD_BW_0",
        "SVD_BW_180": "SVD_BW_180",
        "BP_FW_325": "BP_FW_325",
        "BP_FW_215": "BP_FW_215",
        "BP_BW_325": "BP_BW_325",
        "BP_BW_215": "BP_BW_215",
        "SVD_FW_60": "SVD_FW_60",
        "SVD_FW_120": "SVD_FW_120",
        "SVD_FW_240": "SVD_FW_240",
        "SVD_FW_300": "SVD_FW_300",
        "QCS_BW_45": "QCS_BW_45",
        "QCS_BW_135": "QCS_BW_135",
        "QCS_BW_225": "QCS_BW_225",
        "QCS_BW_315": "QCS_BW_315",
        "QCS_FW_45": "QCS_FW_45",
        "QCS_FW_135": "QCS_FW_135",
        "QCS_FW_225": "QCS_FW_225",
        "QCS_FW_315": "QCS_FW_315"}

    configuration = {"definition": diamondsDefinition, "dataFiles": "",
                     "studyStart": startTime, "studyEnd": endTime}

    configurationFile = {"configuration": configuration,
                         "configurationToBeUsed": "configuration"
                         }
    # Create a file in JSON
    with open('data.txt', 'w') as outfile:
        json.dump(configurationFile, outfile, sort_keys=True, indent=4, separators=(',', ': '))
    return 'data.txt'


def main():
    now = datetime.now()
    yesterday = now - timedelta(hours=24)
    print now
    print yesterday
    start = now.strftime("%Y-%m-%d %H:%M:%S")
    end = yesterday.strftime("%Y-%m-%d %H:%M:%S")
    configurationFile = generateConfigurationJson(start, end)
    configuration = Configuration(configurationFile)
    configuration.load()
    analisysCore = AnalysisCore(configuration, "")
    analisysCore.setPedestalZero()
    #analisysCore.archiveRequest("http://localhost:17668/retrieval")

    label = "%s - Integrated dose = %s"
    row = ["from " + end + " to " + start]
    for address, diamondLabel in configuration.getDiamondsLabels().iteritems():
        row.append(analisysCore.getIntegratedDose(address))
        print label % (diamondLabel, analisysCore.getIntegratedDose(address))

    scope = ['https://spreadsheets.google.com/feeds',
             'https://www.googleapis.com/auth/drive']

    credentials = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)

    gc = gspread.authorize(credentials)

    wks = gc.open("integrated dose summary - from Oct 2019").sheet1

    wks.append_row(row, 'RAW')


if __name__ == "__main__":
    main()
