from array import array
from datetime import datetime
import time

from ROOT import TGraph, TH1F, TCanvas, TLegend


class Diamond:
    def __init__(self, name):
        self.label = name
        self.pedestal = "None"
        self.doseRate_vec = array('d')
        self.timeStamp_vec = list()
        self.millisecond_vec = array('d')
        self.doseRatePedestal_vec = array('d')
        self.timeStampPedestal_vec = list()
        self.millisecondPedestal_vec = array('d')

    def getLabel(self):
        return self.label

    def add(self, doseRate, timeStamp, milliseconds):
        self.doseRate_vec.append(doseRate)
        self.timeStamp_vec.append(timeStamp)
        self.millisecond_vec.append(milliseconds)

    def addPedestal(self, doseRate, timeStamp, milliseconds):
        self.doseRatePedestal_vec.append(doseRate)
        self.timeStampPedestal_vec.append(timeStamp)
        self.millisecondPedestal_vec.append(milliseconds)

    def correlaionPlot(self, otherDiamond, startTime, endTime):
        doseRate_double = doseRateOther_double = array('d')
        startTimePlot = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
        endTimePlot = datetime.strptime(endTime, "%Y-%m-%d %H:%M:%S")
        for index in range(len(self.timeStamp_vec)):
            if (self.timeStamp_vec[index] <= endTimePlot and self.timeStamp_vec[index] >= startTimePlot):
                doseRate_double.append(self.doseRate_vec[index])
                doseRateOther_double.append(otherDiamond.doseRate_vec[index])
        gr = TGraph(len(doseRate_double), doseRate_double, doseRateOther_double)
        gr.SetLineColor(2)
        gr.SetLineWidth(4)
        gr.SetMarkerColor(4)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.GetXaxis().SetTitleOffset(1.4)
        gr.GetYaxis().SetTitleOffset(1.4)
        gr.GetXaxis().SetTitle("dose rate - " + self.getLabel() + " [mRad/s]")
        gr.GetYaxis().SetTitle("dose rate - " + otherDiamond.getLabel() + " [mRad/s]")
        return gr

    def getIntegratedDose(self, startTime, endTime):
        if (self.pedestal == "None"):
            raise Exception('You have to call pedestalEvaluation or setPedestal method first!')

        timeStamp_subSet = list()
        milliseconds_double = array('d')
        doseRate_double = array('d')
        startTimePlot = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
        endTimePlot = datetime.strptime(endTime, "%Y-%m-%d %H:%M:%S")
        for index in range(len(self.timeStamp_vec)):
            if (self.timeStamp_vec[index] <= endTimePlot and self.timeStamp_vec[index] >= startTimePlot):
                timeStamp_subSet.append(self.timeStamp_vec[index])
                doseRate_double.append(self.doseRate_vec[index])
                milliseconds_double.append(self.millisecond_vec[index])

        integrateDose = 0
        for i in range(len(doseRate_double) - 1):
            diffTimeStamp = timeStamp_subSet[i + 1] - timeStamp_subSet[i]
            diffMilliseconds = milliseconds_double[i + 1] - milliseconds_double[i]
            seconds = diffTimeStamp.total_seconds()
            totalMilliseconds = seconds + diffMilliseconds
            integrateDose = integrateDose + (doseRate_double[i] - self.pedestal) * totalMilliseconds
        return integrateDose

    def plot(self, startTime, endTime):
        if (self.pedestal == "None"):
            raise Exception('You have to call pedestalEvaluation or setPedestal method first!')

        timeStamp_double = array('d')
        doseRate_double = array('d')
        startTimePlot = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
        endTimePlot = datetime.strptime(endTime, "%Y-%m-%d %H:%M:%S")
        for index in range(len(self.timeStamp_vec)):
            if (self.timeStamp_vec[index] <= endTimePlot and self.timeStamp_vec[index] >= startTimePlot):
                timeStamp_double.append(float(self.timeStamp_vec[index].strftime("%s")))
                doseRate_double.append(self.doseRate_vec[index] - self.pedestal)
        if (len(doseRate_double) == 0):
            return None
        gr = TGraph(len(timeStamp_double), timeStamp_double, doseRate_double)
        gr.SetName(self.label)
        gr.SetLineColor(2)
        gr.SetLineWidth(4)
        gr.SetMarkerColor(4)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.SetTitle(str(self.label))
        gr.GetXaxis().SetTimeDisplay(1)
        gr.GetXaxis().SetNdivisions(5, 5, 0, 0)
        # gr.GetXaxis().SetTimeFormat("#splitline{%m-%d}{h%H}%F1970-01-01 00:00:00")
        gr.GetXaxis().SetTimeFormat("#splitline{}{%H:%M:%S}%F1970-01-01 00:00:00 ")
        gr.GetXaxis().SetLabelOffset(0.02)
        gr.GetXaxis().SetTitleOffset(1.4)
        gr.GetYaxis().SetTitleOffset(1.4)
        gr.GetXaxis().SetTitle("date [h-m]")
        gr.GetYaxis().SetTitle('dose rate [mRad/s]')
        legend = TLegend(0.5, 0.5, 0.75, 0.75)
        legend.AddEntry(gr, self.label, "p")
        legend.Draw("SAME")
        return gr

    def plotRawData_noPedestal(self, startTime, endTime):
        timeStamp_double = array('d')
        doseRate_double = array('d')
        startTimePlot = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
        endTimePlot = datetime.strptime(endTime, "%Y-%m-%d %H:%M:%S")
        for index in range(len(self.timeStamp_vec)):
            if (self.timeStamp_vec[index] <= endTimePlot and self.timeStamp_vec[index] >= startTimePlot):
                timeStamp_double.append(float(self.timeStamp_vec[index].strftime("%s")))
                doseRate_double.append(self.doseRate_vec[index])
        if (len(doseRate_double) == 0):
            return None
        gr = TGraph(len(timeStamp_double), timeStamp_double, doseRate_double)
        gr.SetName(self.label + "_raw_noPedSubtracted")
        gr.SetLineColor(2)
        gr.SetLineWidth(4)
        gr.SetMarkerColor(4)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.SetTitle(str(self.label))
        gr.GetXaxis().SetTimeDisplay(1)
        gr.GetXaxis().SetNdivisions(5, 5, 0, 0)
        # gr.GetXaxis().SetTimeFormat("#splitline{%m-%d}{h%H}%F1970-01-01 00:00:00")
        gr.GetXaxis().SetTimeFormat("#splitline{}{%H:%M:%S}%F1970-01-01 00:00:00 ")
        gr.GetXaxis().SetLabelOffset(0.02)
        gr.GetXaxis().SetTitleOffset(1.4)
        gr.GetYaxis().SetTitleOffset(1.4)
        gr.GetXaxis().SetTitle("date [h-m]")
        gr.GetYaxis().SetTitle('dose rate no pedestal subratction [mRad/s]')
        legend = TLegend(0.5, 0.5, 0.75, 0.75)
        legend.AddEntry(gr, self.label, "p")
        legend.Draw("SAME")
        return gr

    def multiPlot(self, startTime, endTime, legend, count):
        if (self.pedestal == "None"):
            raise Exception('You have to call pedestalEvaluation or setPedestal method first!')
        # colours = ["kBlack", "kBlue", "kRed", "kMagenta"]
        timeStamp_double = array('d')
        doseRate_double = array('d')
        startTimePlot = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
        endTimePlot = datetime.strptime(endTime, "%Y-%m-%d %H:%M:%S")
        for index in range(len(self.timeStamp_vec)):
            if (self.timeStamp_vec[index] <= endTimePlot and self.timeStamp_vec[index] >= startTimePlot):
                timeStamp_double.append(float(self.timeStamp_vec[index].strftime("%s")))
                doseRate_double.append(self.doseRate_vec[index] - self.pedestal)
        if (len(doseRate_double) == 0):
            return None
        col = count + 1
        gr = TGraph(len(timeStamp_double), timeStamp_double, doseRate_double)
        gr.SetLineColor(col)
        gr.SetLineWidth(4)
        gr.SetMarkerColor(col)
        gr.SetMarkerStyle(21)
        gr.SetMarkerSize(0.35)
        gr.SetTitle(str(self.label))
        gr.GetXaxis().SetTimeDisplay(1)
        gr.GetXaxis().SetNdivisions(5, 5, 0, 0)
        # gr.GetXaxis().SetTimeFormat("#splitline{%m-%d}{h%H}%F1970-01-01 00:00:00")
        gr.GetXaxis().SetTimeFormat("#splitline{}{%H:%M:%S}%F1970-01-01 00:00:00 ")
        gr.GetXaxis().SetLabelOffset(0.02)
        gr.GetXaxis().SetTitleOffset(1.4)
        gr.GetYaxis().SetTitleOffset(1.4)
        gr.GetXaxis().SetTitle("date [h-m]")
        gr.GetYaxis().SetTitle('dose rate [mRad/s]')
        l1 = legend.AddEntry(gr, self.label, "p")
        l1.SetTextColor(col)
        return gr

    def pedestalEvaluation(self, initDate, endDate):
        startTime = datetime.strptime(initDate, "%Y-%m-%d %H:%M:%S")
        endTime = datetime.strptime(endDate, "%Y-%m-%d %H:%M:%S")
        max, min = self.getMinMax(endTime, startTime)
        c13 = TCanvas('c13', ' histo', 900, 600)
        histo = TH1F("histo", "histo", 100, min, max)

        elements = pedestal = 0
        for i in range(len(self.doseRatePedestal_vec)):
            if self.timeStampPedestal_vec[i] <= endTime and self.timeStampPedestal_vec[i] >= startTime:
                histo.Fill(self.doseRatePedestal_vec[i])
                elements = elements + 1
                pedestal = pedestal + self.doseRatePedestal_vec[i]
        if elements != 0:
            pedestal = pedestal / elements
            pedestalToPrint = "pedestal of %s is %f"
            print pedestalToPrint % (self.label, pedestal)
        histo.Draw()
        histo.SaveAs("histo_" + self.label + ".root")
        self.pedestal = pedestal
        return pedestal

    def getMinMax(self, endTime, startTime):
        min, max = 1000000, -1000000
        for i in range(len(self.doseRatePedestal_vec)):
            if self.timeStampPedestal_vec[i] <= endTime and self.timeStampPedestal_vec[i] >= startTime:
                if (self.doseRatePedestal_vec[i] < min):
                    min = self.doseRatePedestal_vec[i]
                if (self.doseRatePedestal_vec[i] > max):
                    max = self.doseRatePedestal_vec[i]
        return max, min

    def setPedestal(self, pedestal):
        self.pedestal = pedestal

    def getDoseRate(self):
        return self.doseRate_vec

    def getPedestal(self):
        pedestalVec = array('d')
        for i in range(0, len(self.doseRate_vec)):
            pedestalVec.append(self.pedestal)
        return pedestalVec

    def getTimestampUnixtime(self):
        timestampUnixtime = array('d')
        for i in range(0, len(self.timeStamp_vec)):
            timestampUnixtime.append(time.mktime(self.timeStamp_vec[i].timetuple()) + self.millisecond_vec[i])
        return timestampUnixtime
