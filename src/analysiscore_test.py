import os
import unittest

from analysisCore import *


class TestAnalysisCore(unittest.TestCase):
    def test_getmultiplot(self):
        conf = Configuration("")
        conf.configurationToBeUsed[DEFINITION] = {
            "BP_FW_145": "BP_FW_145",
            "BP_FW_35": "BP_FW_35",
            "BP_BW_145": "BP_BW_145",
            "BP_BW_35": "BP_BW_35",
            "SVD_FW_0": "SVD_FW_0",
            "SVD_FW_180": "SVD_FW_180",
            "SVD_BW_0": "SVD_BW_0",
            "SVD_BW_180": "SVD_BW_180",
            "SVD_BW_60": "SVD_BW_60",
            "SVD_BW_120": "SVD_BW_120",
            "SVD_BW_240": "SVD_BW_240",
            "SVD_BW_300": "SVD_BW_300",
            "BP_FW_325": "BP_FW_325",
            "BP_FW_215": "BP_FW_215",
            "BP_BW_325": "BP_BW_325",
            "BP_BW_215": "BP_BW_215",
            "SVD_FW_60": "SVD_FW_60",
            "SVD_FW_120": "SVD_FW_120",
            "SVD_FW_240": "SVD_FW_240",
            "SVD_FW_300": "SVD_FW_300",
            "QCS_BW_45": "QCS_BW_45",
            "QCS_BW_135": "QCS_BW_135",
            "QCS_BW_225": "QCS_BW_225",
            "QCS_BW_315": "QCS_BW_315",
            "QCS_FW_45": "QCS_FW_45",
            "QCS_FW_135": "QCS_FW_135",
            "QCS_FW_225": "QCS_FW_225",
            "QCS_FW_315": "QCS_FW_315"}
        conf.configurationToBeUsed[STUDY_START] = "2019-03-12 00:48:25"
        conf.configurationToBeUsed[STUDY_END] = "2019-03-12 00:48:27"

        analysis = AnalysisCore(conf, "")
        analysis.readFile("../resources/firstBeamFileTest")
        analysis.evaluatePedestalOn("2019-03-12 00:48:25", "2019-03-12 00:48:27")
        test = ["QCS_FW_45", "QCS_FW_135", "QCS_FW_225", "QCS_FW_315"]
        analysis.createDoseRateVSTimePlotGroup(test, "pippo", "2019-03-11 16:48:25", "2019-03-11 16:48:27")

    def test_getIntegratedDose(self):
        conf = Configuration("")
        conf.configurationToBeUsed[DEFINITION] = {
            "BP_FW_145": "BP_FW_145",
            "BP_FW_35": "BP_FW_35",
            "BP_BW_145": "BP_BW_145",
            "BP_BW_35": "BP_BW_35",
            "SVD_FW_0": "SVD_FW_0",
            "SVD_FW_180": "SVD_FW_180",
            "SVD_BW_0": "SVD_BW_0",
            "SVD_BW_180": "SVD_BW_180",
            "SVD_BW_60": "SVD_BW_60",
            "SVD_BW_120": "SVD_BW_120",
            "SVD_BW_240": "SVD_BW_240",
            "SVD_BW_300": "SVD_BW_300",
            "BP_FW_325": "BP_FW_325",
            "BP_FW_215": "BP_FW_215",
            "BP_BW_325": "BP_BW_325",
            "BP_BW_215": "BP_BW_215",
            "SVD_FW_60": "SVD_FW_60",
            "SVD_FW_120": "SVD_FW_120",
            "SVD_FW_240": "SVD_FW_240",
            "SVD_FW_300": "SVD_FW_300",
            "QCS_BW_45": "QCS_BW_45",
            "QCS_BW_135": "QCS_BW_135",
            "QCS_BW_225": "QCS_BW_225",
            "QCS_BW_315": "QCS_BW_315",
            "QCS_FW_45": "QCS_FW_45",
            "QCS_FW_135": "QCS_FW_135",
            "QCS_FW_225": "QCS_FW_225",
            "QCS_FW_315": "QCS_FW_315"}
        conf.configurationToBeUsed[STUDY_START] = "2019-03-12 00:48:25"
        conf.configurationToBeUsed[STUDY_END] = "2019-03-12 00:48:27"

        analysis = AnalysisCore(conf, "")
        analysis.readFile("../resources/firstBeamFileTest")
        analysis.evaluatePedestalOn("2019-03-12 00:48:25", "2019-03-12 00:48:27")

        integratedDose = analysis.getIntegratedDose("QCS_FW_45")
        self.assertAlmostEqual(540.9318115, integratedDose)
        integratedDose = analysis.getIntegratedDose("QCS_FW_135")
        self.assertAlmostEqual(181.2886231, integratedDose)
        integratedDose = analysis.getIntegratedDose("QCS_FW_225")
        self.assertAlmostEqual(178.6457946, integratedDose)
        integratedDose = analysis.getIntegratedDose("QCS_FW_315")
        self.assertAlmostEqual(846.9072998, integratedDose)

    def test_createDoseRateVSTimePlot(self):
        conf = Configuration("")
        conf.configurationToBeUsed[DEFINITION] = {
            "BP_FW_145": "BP_FW_145",
            "BP_FW_35": "BP_FW_35",
            "BP_BW_145": "BP_BW_145",
            "BP_BW_35": "BP_BW_35",
            "SVD_FW_0": "SVD_FW_0",
            "SVD_FW_180": "SVD_FW_180",
            "SVD_BW_0": "SVD_BW_0",
            "SVD_BW_180": "SVD_BW_180",
            "SVD_BW_60": "SVD_BW_60",
            "SVD_BW_120": "SVD_BW_120",
            "SVD_BW_240": "SVD_BW_240",
            "SVD_BW_300": "SVD_BW_300",
            "BP_FW_325": "BP_FW_325",
            "BP_FW_215": "BP_FW_215",
            "BP_BW_325": "BP_BW_325",
            "BP_BW_215": "BP_BW_215",
            "SVD_FW_60": "SVD_FW_60",
            "SVD_FW_120": "SVD_FW_120",
            "SVD_FW_240": "SVD_FW_240",
            "SVD_FW_300": "SVD_FW_300",
            "QCS_BW_45": "QCS_BW_45",
            "QCS_BW_135": "QCS_BW_135",
            "QCS_BW_225": "QCS_BW_225",
            "QCS_BW_315": "QCS_BW_315",
            "QCS_FW_45": "QCS_FW_45",
            "QCS_FW_135": "QCS_FW_135",
            "QCS_FW_225": "QCS_FW_225",
            "QCS_FW_315": "QCS_FW_315"}
        conf.configurationToBeUsed[STUDY_START] = "2019-03-12 00:48:25"
        conf.configurationToBeUsed[STUDY_END] = "2019-03-12 00:48:27"

        analysis = AnalysisCore(conf, "")
        analysis.readFile("../resources/firstBeamFileTest")
        analysis.evaluatePedestalOn("2019-03-12 00:48:25", "2019-03-12 00:48:27")
        analysis.diamondsMap["BP_FW_145"].setPedestal(0)

        analysis.createDoseRateVSTimePlot("BP_FW_145", "2019-03-12 00:48:25", "2019-03-12 00:48:27")
        # TODO cambiare nome file con il nome vero del diamante
        self.assertTrue(os.path.isfile('./BP_FW_145_doseRateVsTime.root'))

    def test_pedestalEvaluation(self):
        conf = Configuration("")
        conf.configurationToBeUsed[DEFINITION] = {
            "BP_FW_145": "BP_FW_145",
            "BP_FW_35": "BP_FW_35",
            "BP_BW_145": "BP_BW_145",
            "BP_BW_35": "BP_BW_35",
            "SVD_FW_0": "SVD_FW_0",
            "SVD_FW_180": "SVD_FW_180",
            "SVD_BW_0": "SVD_BW_0",
            "SVD_BW_180": "SVD_BW_180",
            "SVD_BW_60": "SVD_BW_60",
            "SVD_BW_120": "SVD_BW_120",
            "SVD_BW_240": "SVD_BW_240",
            "SVD_BW_300": "SVD_BW_300",
            "BP_FW_325": "BP_FW_325",
            "BP_FW_215": "BP_FW_215",
            "BP_BW_325": "BP_BW_325",
            "BP_BW_215": "BP_BW_215",
            "SVD_FW_60": "SVD_FW_60",
            "SVD_FW_120": "SVD_FW_120",
            "SVD_FW_240": "SVD_FW_240",
            "SVD_FW_300": "SVD_FW_300",
            "QCS_BW_45": "QCS_BW_45",
            "QCS_BW_135": "QCS_BW_135",
            "QCS_BW_225": "QCS_BW_225",
            "QCS_BW_315": "QCS_BW_315",
            "QCS_FW_45": "QCS_FW_45",
            "QCS_FW_135": "QCS_FW_135",
            "QCS_FW_225": "QCS_FW_225",
            "QCS_FW_315": "QCS_FW_315"}
        conf.configurationToBeUsed[STUDY_START] = "2019-03-12 00:48:25"
        conf.configurationToBeUsed[STUDY_END] = "2019-03-12 00:48:27"

        analysis = AnalysisCore(conf, "")
        analysis.readFile("../resources/firstBeamFileTest")
        pedestalVec = analysis.evaluatePedestalOn("2019-03-12 00:48:25", "2019-03-12 00:48:27")

        self.assertAlmostEqual(2686.39312261538, pedestalVec['SVD_BW_60'])
        self.assertAlmostEqual(906.294663653846, pedestalVec['SVD_BW_120'])
        self.assertAlmostEqual(892.831728307692, pedestalVec['SVD_BW_240'])
        self.assertAlmostEqual(4881.06940165385, pedestalVec['SVD_BW_300'])

    def test_integratedDoseWithPedestal(self):
        conf = Configuration("")
        conf.configurationToBeUsed[DEFINITION] = {
            "BP_FW_145": "BP_FW_145",
            "BP_FW_35": "BP_FW_35",
            "BP_BW_145": "BP_BW_145",
            "BP_BW_35": "BP_BW_35",
            "SVD_FW_0": "SVD_FW_0",
            "SVD_FW_180": "SVD_FW_180",
            "SVD_BW_0": "SVD_BW_0",
            "SVD_BW_180": "SVD_BW_180",
            "SVD_BW_60": "SVD_BW_60",
            "SVD_BW_120": "SVD_BW_120",
            "SVD_BW_240": "SVD_BW_240",
            "SVD_BW_300": "SVD_BW_300",
            "BP_FW_325": "BP_FW_325",
            "BP_FW_215": "BP_FW_215",
            "BP_BW_325": "BP_BW_325",
            "BP_BW_215": "BP_BW_215",
            "SVD_FW_60": "SVD_FW_60",
            "SVD_FW_120": "SVD_FW_120",
            "SVD_FW_240": "SVD_FW_240",
            "SVD_FW_300": "SVD_FW_300",
            "QCS_BW_45": "QCS_BW_45",
            "QCS_BW_135": "QCS_BW_135",
            "QCS_BW_225": "QCS_BW_225",
            "QCS_BW_315": "QCS_BW_315",
            "QCS_FW_45": "QCS_FW_45",
            "QCS_FW_135": "QCS_FW_135",
            "QCS_FW_225": "QCS_FW_225",
            "QCS_FW_315": "QCS_FW_315"}
        conf.configurationToBeUsed[STUDY_START] = "2019-03-12 00:48:25"
        conf.configurationToBeUsed[STUDY_END] = "2019-03-12 00:48:27"

        analysis = AnalysisCore(conf, "")
        analysis.readFile("../resources/firstBeamFileTest")
        analysis.evaluatePedestalOn("2019-03-12 00:48:25", "2019-03-12 00:48:27")

        integratedDose = analysis.getIntegratedDose("BP_FW_145")
        self.assertAlmostEqual(1.94533689999998, integratedDose)

    def test_generateConfigurationJson(self):
        diamondsDefinition1 = {'DCU1ch0': "SVD_BW_60",
                               'DCU1ch1': "SVD_BW_120",
                               'DCU1ch2': "SVD_BW_240",
                               'DCU1ch3': "SVD_BW_300"}
        dataFiles1 = ['file1', 'file2', 'file3']
        configuration1 = {"definition": diamondsDefinition1, "dataFiles": dataFiles1,
                          "pedistalInit": "2019-03-06 20:13:12", "pedistalEnd": "2019-03-06 20:13:14",
                          "studyInit": "2019-03-06 20:13:12", "studylEnd": "2019-03-06 20:13:14"}

        diamondsDefinition2 = {'DCU1ch0': "SVD_BW_60",
                               'DCU1ch1': "SVD_BW_120",
                               'DCU1ch2': "SVD_BW_240",
                               'DCU1ch3': "SVD_BW_300"}

        dataFiles2 = ['file1', 'file2', 'file3']
        configuration2 = {"definition": diamondsDefinition2, "dataFiles": dataFiles2,
                          "pedistalInit": "2019-03-06 20:13:12", "pedistalEnd": "2019-03-06 20:13:14",
                          "studyInit": "2019-03-06 20:13:12", "studylEnd": "2019-03-06 20:13:14"}

        configurationFile = {"configuration1": configuration1,
                             "configuration2": configuration2,
                             "configurationToBeUsed": "configuration2"
                             }

        # Create a file in JSON
        with open('data.txt', 'w') as outfile:
            json.dump(configurationFile, outfile, sort_keys=True, indent=4, separators=(',', ': '))


if __name__ == '__main__':
    unittest.main()
