import os
import re
import sys
import time

from getfromarchiver import *

import ROOT as r
from ROOT import TCanvas, TLegend, SetOwnership, TFile, TTree

from configuration import *
from diamond import *


class AnalysisCore():
    pedestalTimeTable = {}
    diamondsMap = {}

    def __init__(self, configuration, pedestalFile):
        self.configuration = configuration
        self.pedestalFile = pedestalFile
        self.loadPedestals(pedestalFile)

        diamonds = configuration.getDiamondsLabels()
        for phisicalAddress, label in diamonds.iteritems():
            self.diamondsMap[phisicalAddress] = Diamond(label)

    def readFile(self, dataFile):
        print dataFile
        infile = open(dataFile, "r")
        for line in infile:
            self.process(line)

        print "End of reading file"

    def process(self, line):
        parts = re.compile(":|\s+").split(line)
        name = parts[2]
        data = parts[4]
        if (data.startswith("***")):
            return
        hour = parts[5]
        minutes = parts[6]
        secondsAndMilliseconds = parts[7]
        doseRate = parts[8]

        token = secondsAndMilliseconds.split(".")
        seconds = token[0]
        millisecondsIntegerPart = token[1]
        zeroPoint = "0.%s"
        milliseconds = zeroPoint % (millisecondsIntegerPart)

        dataFormat = "%s %s:%s:%s"
        dataTime = dataFormat % (data, hour, minutes, seconds)
        timeStamp = datetime.strptime(dataTime, "%Y-%m-%d %H:%M:%S")

        diamond = self.getDiamond(name)

        diamond.add(float(doseRate), timeStamp, float(milliseconds))

    def getIntegratedDose(self, phisicalAddress):
        diamond = self.getDiamond(phisicalAddress)
        integratedDose = diamond.getIntegratedDose(self.configuration.getStudyStart(), self.configuration.getStudyEnd())
        return integratedDose

    def getDiamond(self, dcuAndChannel):
        return self.diamondsMap.get(dcuAndChannel)

    def createDoseRateVSTimePlot(self, physicalAddress, startTime, endTime):
        diamond = self.getDiamond(physicalAddress)
        name = "%s_doseRateVsTime"
        c9 = TCanvas('c9', name % (diamond.getLabel()), 900, 600)
        plot = diamond.plot(startTime, endTime)
        if (plot is None):
            return 0
        plot.Draw()
        plot.Write()
        return 0

    def createDoseRateVSTimePlot_Raw_noPedestal(self, physicalAddress, startTime, endTime):
        diamond = self.getDiamond(physicalAddress)
        name = "%s_doseRateVsTime"
        c = TCanvas('c', name % (diamond.getLabel()), 900, 600)
        plot = diamond.plotRawData_noPedestal(startTime, endTime)
        if (plot is None):
            return 0
        plot.Draw()
        plot.Write()
        return 0

    def createDoseRateVSTimePlotGroup(self, group, groupName, startTime, endTime):
        name = "%s_doseRateVsTime"
        count = 0
        c9 = TCanvas('c9', name % (groupName), 900, 600)
        plots = []
        legend = TLegend(0.5, 0.5, 0.75, 0.75)
        SetOwnership(legend, False)
        for diamondPhysicalAddress in group:
            diamond = self.getDiamond(diamondPhysicalAddress)
            plots.insert(count, diamond.multiPlot(startTime, endTime, legend, count))
            if (plots[count] is None):
                continue
            if (count == 0):
                plots[count].Draw("AP")
            else:
                plots[count].Draw("Same")
            count = count + 1
        legend.Draw()
        c9.SaveAs(name % (groupName + startTime.replace(" ", "_")) + ".root")
        return 0

    def createCorrelationPlot(self, startTime, endTime):
        diamond1 = self.getDiamond("QCS_FW_45")
        diamond2 = self.getDiamond("QCS_FW_135")
        plot = diamond1.correlaionPlot(diamond2, startTime, endTime)
        c9 = TCanvas('c9', (diamond1.getLabel()), 900, 600)
        if (plot is None):
            return 0
        plot.Draw("AP")
        plot.SaveAs(diamond1.getLabel() + "-" + diamond2.getLabel() + "_correlation.root")
        return 0

    def evaluatePedestalOn(self, startTime, endTime):
        pedestals = {}
        for diamond in self.diamondsMap.itervalues():
            pedestals.update({diamond.label: diamond.pedestalEvaluation(startTime, endTime)})
        self.pedestalTimeTable.update({startTime: pedestals})
        self.savePedestals()
        return pedestals

    def loadPedestals(self, pedestalFile):
        # TODO ricarica file pedestals
        pass

    def savePedestals(self):
        # TODO salva su file i pedestals
        pass

    def archiveRequest(self, param):
        # from '2019-03-26T18:07:00.000+0900'
        fetchData = GetFromArchiver(param)

        endTimePed = self.configuration.getPedestalEnd()
        startTimePed = self.configuration.getPedestalStart()
        if startTimePed != None or endTimePed != None:
            startTimePed = datetime.strptime(startTimePed, "%Y-%m-%d %H:%M:%S")
            endTimePed = datetime.strptime(endTimePed, "%Y-%m-%d %H:%M:%S")
            tRangePed = time.mktime(startTimePed.timetuple()), time.mktime(endTimePed.timetuple())
            for diamond in self.diamondsMap.itervalues():
                pedData = fetchData.fetchData(tRangePed, "VXD:Rad:%s:DoseRate" % diamond.getLabel())
                self.storeData(diamond.getLabel() + "_pedestal.json", pedData)
                for sample in pedData["data"]:
                    nanos = sample["nanos"]
                    timeStamp = datetime.strptime(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(sample["secs"])),
                                                  "%Y-%m-%d %H:%M:%S")
                    value = sample["val"]
                    diamond.addPedestal(value, timeStamp, float(nanos) / 1000000000)

        endTime = self.configuration.getStudyEnd()
        startTime = self.configuration.getStudyStart()
        startTimePlot = datetime.strptime(startTime, "%Y-%m-%d %H:%M:%S")
        endTimePlot = datetime.strptime(endTime, "%Y-%m-%d %H:%M:%S")
        tRangeStudy = time.mktime(startTimePlot.timetuple()), time.mktime(endTimePlot.timetuple())
        for diamond in self.diamondsMap.itervalues():
            studyData = fetchData.fetchData(tRangeStudy, "VXD:Rad:%s:DoseRate" % diamond.getLabel())
            self.storeData(diamond.getLabel() + "_data.json", studyData)
            for sample in studyData["data"]:
                nanos = sample["nanos"]
                timeStamp = datetime.strptime(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(sample["secs"])),
                                              "%Y-%m-%d %H:%M:%S")
                value = sample["val"]
                diamond.add(value, timeStamp, float(nanos) / 1000000000)

        pass

    def storeData(self, label, data):
        if self.configuration.getStoreData() == 'True':
            with open("serverData/" + label, 'w') as outfile:
                json.dump(data, outfile, sort_keys=True, indent=4, separators=(',', ': '))

    def doNtuple(self, initTime, endTime):
        if self.configuration.getDoNtuple() == 'True':
            doseRatesVectors = []
            timeStampVectors = []
            pedestalVectors = []
            filenameNtuple = "DiaNtuple_from_" + initTime + "_to_" + endTime + ".root"
            ntuple = TFile(filenameNtuple, "RECREATE")
            t = TTree('tout', 'tree')
            for diamond in self.diamondsMap.itervalues():
                doseRateData = diamond.getDoseRate()
                timeStampData = diamond.getTimestampUnixtime()
                pedestalData = diamond.getPedestal()
                doseRate = r.vector('float')(len(doseRateData))
                timestamp = r.vector('double')(len(timeStampData))
                pedestal = r.vector('float')(len(pedestalData))

                doseRatesVectors.append(doseRate)
                timeStampVectors.append(timestamp)
                pedestalVectors.append(pedestal)

                t.Branch(str(diamond.label) + "_doseRate", doseRatesVectors[-1])
                t.Branch(str(diamond.label) + "_timestamp", timeStampVectors[-1])
                t.Branch(str(diamond.label) + "_pedestal", pedestalVectors[-1])

                self.convertToRootVector(timeStampData, timeStampVectors[-1])
                self.convertToRootVector(doseRateData, doseRatesVectors[-1])
                self.convertToRootVector(pedestalData, pedestalVectors[-1])
            t.Fill()

            print("Num of entries: " + str(t.GetEntries()))

            ntuple.Write()
            ntuple.Close()

            self.readVector('SVD_FW_120_doseRate', filenameNtuple)

    def readVector(self, branchName, filenameNtuple):
        fi = r.TFile(filenameNtuple)
        tree = fi.Get('tout')
        vec = r.vector('float')()
        tree.SetBranchAddress(branchName, vec)
        tree.GetEntry(0)
        print("Vector size: " + str(vec.size()))
        return vec

    def convertToRootVector(self, dataVec, doseRate):
        for i in range(0, len(dataVec)):
            doseRate[i] = float(dataVec[i])
        return doseRate

    def setPedestalZero(self):
        for diamond in self.diamondsMap.itervalues():
            diamond.setPedestal(0)


def main():
    configurationFile = sys.argv[1]
    configuration = Configuration(configurationFile)
    configuration.load()
    analisysCore = AnalysisCore(configuration, "")

    fileList = configuration.getDataFiles()
    if len(fileList) == 0:
        analisysCore.archiveRequest("http://localhost:17668/retrieval")
    else:
        for file in fileList:
            analisysCore.readFile(file)

    analisysCore.evaluatePedestalOn(configuration.getPedestalStart(), configuration.getPedestalEnd())

    label = "%s - Integrated dose = %s"
    filename = "DoseRate_value_vs_time_from_" + str((configuration.getStudyStart()).replace(" ", "_")) + "_to_" + str(
        (configuration.getStudyEnd()).replace(" ", "_")) + ".root"
    fout = TFile(filename, "UPDATE")
    for address, diamondLabel in configuration.getDiamondsLabels().iteritems():
        print label % (diamondLabel, analisysCore.getIntegratedDose(address))

        analisysCore.createDoseRateVSTimePlot(address, configuration.getStudyStart(), configuration.getStudyEnd())
        analisysCore.createDoseRateVSTimePlot_Raw_noPedestal(address, configuration.getStudyStart(),
                                                             configuration.getStudyEnd())

    fout.Write()
    fout.Close()

    # analisysCore.doNtuple((configuration.getStudyStart()).replace(" ", "_"),
    #                     (configuration.getStudyEnd()).replace(" ", "_"))
    # analisysCore.createCorrelationPlot(configuration.getStudyStart(), configuration.getStudyEnd())

    # da scommentare da qui
    # groups = configuration.getGroups()
    # for key, group in groups.iteritems():
    #    analisysCore.createDoseRateVSTimePlotGroup(group, key, configuration.getStudyStart(),
    #                                               configuration.getStudyEnd())
    # fino a qui


if __name__ == "__main__":
    main()
