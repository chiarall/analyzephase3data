import json

CONFIGURATION_TO_BE_USED = 'configurationToBeUsed'
DATA_FILES = 'dataFiles'
DEFINITION = 'definition'
PEDISTAL_START = 'pedistalStart'
PEDISTAL_END = 'pedistalEnd'
STUDY_START = 'studyStart'
STUDY_END = 'studyEnd'
STORE_DATA = 'storeData'
DO_NTUPLE = 'doNtuple'


class Configuration(object):
    data = {}
    configurationToBeUsed = {}

    def __init__(self, path):
        self.path = path

    def load(self):
        with open(self.path) as json_file:
            self.data = json.load(json_file)
            configurationToBeUsed = self.data[CONFIGURATION_TO_BE_USED]
            self.configurationToBeUsed = self.data[configurationToBeUsed]

    def getStoreData(self):
        if STORE_DATA in self.configurationToBeUsed:
            return self.configurationToBeUsed[STORE_DATA]
        else:
            return False

    def getDoNtuple(self):
        if DO_NTUPLE in self.configurationToBeUsed:
            return self.configurationToBeUsed[DO_NTUPLE]
        else:
            return False

    def getPedestalStart(self):
        if PEDISTAL_START in self.configurationToBeUsed:
            return self.configurationToBeUsed[PEDISTAL_START]
        else:
            return None

    def getDiamondsLabels(self):
        return self.configurationToBeUsed[DEFINITION]

    def getDataFiles(self):
        return self.configurationToBeUsed[DATA_FILES]

    def getPedestalEnd(self):
        if PEDISTAL_END in self.configurationToBeUsed:
            return self.configurationToBeUsed[PEDISTAL_END]
        else:
            return None

    def getStudyStart(self):
        return self.configurationToBeUsed[STUDY_START]

    def getStudyEnd(self):
        return self.configurationToBeUsed[STUDY_END]

    def getGroups(self):
        return self.configurationToBeUsed["groups"]
